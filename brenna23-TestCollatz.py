#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, max_cycle

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "1000 2000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,1000)
        self.assertEqual(j,2000)

    def test_read_3(self):
        s = "200 800\n"
        i, j = collatz_read(s)
        self.assertEqual(i,200)
        self.assertEqual(j,800)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 205)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(1001,1005)
        self.assertEqual(v, 143)

    # corner cases
    def test_eval_5(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_6(self):
        v = collatz_eval(500, 2000)
        self.assertEqual(v, 182)

    def test_eval_7(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_8(self):
        v = collatz_eval(55, 55)
        self.assertEqual(v, 113)

    def test_eval_9(self):
        v = collatz_eval(999, 2005)
        self.assertEqual(v, 182)

    def test_eval_10(self):
        v = collatz_eval(3005, 5005)
        self.assertEqual(v, 238)

    def test_eval_11(self):
        v = collatz_eval(1000, 5005)
        self.assertEqual(v, 238)

    def test_eval_15(self):
        v = collatz_eval(1001, 5005)
        self.assertEqual(v, 238)


    # -----
    # max_cycle
    # -----
    
    def test_max_cycle_1(self):
        v = max_cycle(1, 1)
        self.assertEqual(v, 1)

    def test_max_cycle_2(self):
        v = max_cycle(1000, 2000)
        self.assertEqual(v, 182)

    def test_max_cycle_3(self):
        v = max_cycle(185, 207)
        self.assertEqual(v, 120)

    def test_max_cycle_4(self):
        v = max_cycle(3001, 3200)
        self.assertEqual(v, 199)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 10, 20, 300)
        self.assertEqual(w.getvalue(), "10 20 300\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 5, 1000, 1)
        self.assertEqual(w.getvalue(), "5 1000 1\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_3(self):
        r = StringIO("1 10\n100 200\n201 210\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
